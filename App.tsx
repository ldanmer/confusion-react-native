import React from 'react';
// @ts-ignore
import Main from './components/MainComponent';
// @ts-ignore
import { Provider } from 'react-redux';
// @ts-ignore
import { ConfigureStore } from './redux/configureStore';

const store = ConfigureStore();

export default function App() {
    return (
        <Provider store={store}>
            <Main/>
        </Provider>
    );
}

